package com.geosystec.caerm.managers;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geosystec.caerm.listener.EMF;


public class RegistroServiceBean<T> implements RegistroService {
	
	private static final Logger logger = LoggerFactory
			.getLogger(RegistroServiceBean.class);

	EntityManager em;

	public RegistroServiceBean() {
		
		em = EMF.createEntityManager();
		
	}


	public EntityManager getEm() {
		return em;
	}


	public void setEm(EntityManager em) {
		this.em = em;
	}


	@Override
	public Object create(Object t) {
		EntityTransaction tx = null;
		try{
		tx = em.getTransaction();
		tx.begin();
		this.em.persist(t);
		
		//this.em.flush();
		//this.em.refresh(t);
		tx.commit();
		}
		catch(RuntimeException e)
		{
			 if ( tx != null && tx.isActive() ) tx.rollback();
			    throw e; // or display error message
		}
		return t;
	}
	
	public Object createList(List<Object> t) {
		EntityTransaction tx = null;
		try{
		tx = em.getTransaction();
		tx.begin();
		for( int i = 0; i<t.size(); i++)
			this.em.persist(t.get(i));
		
		//this.em.flush();
		//this.em.refresh(t);
		tx.commit();
		}
		catch(Exception e)
		{
			 if ( tx != null && tx.isActive() ) tx.rollback();
			    throw e; // or display error message
		}
		return t;
	}
	
	@Override
	public Object merge(Object t) {
		EntityTransaction tx = null;
		try{
		tx = em.getTransaction();
		tx.begin();
		this.em.merge(t);
		
		//this.em.flush();
		//this.em.refresh(t);
		tx.commit();
		}
		catch(RuntimeException e)
		{
			 if ( tx != null && tx.isActive() ) tx.rollback();
			    throw e; // or display error message
		}
		return t;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object find(Class type, Object id) {
		
		return (T) this.em.find(type, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object update(Object t) {
		EntityTransaction tx = null;
		try{
		tx = em.getTransaction();
		tx.begin();
		this.em.merge(t);
		//this.em.flush();
		//this.em.refresh(t);
		tx.commit();
		
		}
		catch(RuntimeException e)
		{
			 if ( tx != null && tx.isActive() ) tx.rollback();
			    throw e; // or display error message
		}
		return t;
	}
	
	@SuppressWarnings("unchecked")
	public Object updateList(List<Object> t) {
		EntityTransaction tx = null;
		try{
		tx = em.getTransaction();
		tx.begin();
		for( int i=0;i<t.size(); i++){
			this.em.merge(t.get(i));
		}
		//this.em.flush();
		//this.em.refresh(t);
		tx.commit();
		
		}
		catch(Exception e)
		{
			 if ( tx != null && tx.isActive() ) tx.rollback();
			    throw e; // or display error message
		}
		return t;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void delete(Class type, Object id) {
		EntityTransaction tx = null;
		try{
			tx = em.getTransaction();
			tx.begin();
			Object ref = this.em.getReference(type, id);
			logger.debug("ENTRA EN REMOVE REF = "+ref);
		    this.em.remove(ref);
		    tx.commit();
		}catch(RuntimeException e)
		{
			 if ( tx != null && tx.isActive() ) tx.rollback();
			    throw e; // or display error message
		}

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void delete(Object t) {
		EntityTransaction tx = null;
		try{
			tx = em.getTransaction();
			tx.begin();
		    this.em.remove(t);
		    tx.commit();
		}catch(RuntimeException e)
		{
			 if ( tx != null && tx.isActive() ) tx.rollback();
			    throw e; // or display error message
		}

	}
	
	 public List findWithNamedQuery(String namedQueryName){
	        return this.em.createNamedQuery(namedQueryName).getResultList();
	    }
	    
	    public List findWithNamedQuery(String namedQueryName, Map parameters){
	        return findWithNamedQuery(namedQueryName, parameters, 0);
	    }

	    public List findWithNamedQuery(String queryName, int resultLimit) {
	        return this.em.createNamedQuery(queryName).
	                setMaxResults(resultLimit).
	                getResultList();    
	    }

	    public List findByNativeQuery(String sql, Class type) {
	        return this.em.createNativeQuery(sql, type).getResultList();
	    }
	    
	   public List findWithNamedQuery(String namedQueryName, Map parameters,int resultLimit){
	        Set<Entry> rawParameters = parameters.entrySet();
	        Query query = this.em.createNamedQuery(namedQueryName);
	        if(resultLimit > 0)
	            query.setMaxResults(resultLimit);
	        for (Entry entry : rawParameters) {
	           query.setParameter(entry.getKey().toString(), entry.getValue());
	        }
	        return query.getResultList();
	    }

	   
	   public void destroy(){
		   em.close();
		   
	   }
	  
	   


}
