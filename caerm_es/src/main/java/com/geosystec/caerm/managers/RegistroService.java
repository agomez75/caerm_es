package com.geosystec.caerm.managers;

import java.util.List;
import java.util.Map;

public interface RegistroService<T> {
    public T create(T t);
    public T merge(T t);
    public T find(Class type,Object id);
    public T update(T t);
    public void delete(Class type,Object id);
    public void delete(Object t);
    public List findWithNamedQuery(String queryName);
    public List findWithNamedQuery(String queryName,int resultLimit);
    public List findWithNamedQuery(String namedQueryName, Map parameters);
    public List findWithNamedQuery(String namedQueryName, Map parameters,int resultLimit);
}