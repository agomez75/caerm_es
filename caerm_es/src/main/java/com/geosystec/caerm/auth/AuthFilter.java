﻿package com.geosystec.caerm.auth;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.geosystec.caerm.auth.UserBean;

@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.xhtml","/run" })
public class AuthFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		try {
			//System.out.println("doFilter ...");
			HttpServletRequest request = (HttpServletRequest) req;
			HttpServletResponse response = (HttpServletResponse) res;
			HttpSession session = request.getSession(false);
			UserBean user = (UserBean) request.getSession()
					.getAttribute("user");
			
			String rutaLogin = "/inputlogin.xhtml";
			String ruta = request.getRequestURI();
			boolean loginRequest = ruta.contains(rutaLogin);
		//	boolean birtViewerRequest = ruta.contains("run");
		//	System.out.println("ruta3 " + ruta+ " "+birtViewerRequest);
          //  System.out.println("USER ES: "+user);
           // System.out.println("USERLOGIN4 ES: "+user);
			if (loginRequest || (ruta.contains("/javax.faces.resource")) || ((user != null) && (user.getName() != null))) {
				chain.doFilter(request, response);
			}
			
			
			else {
				//System.out.println("loginRequest");
				// User is not logged in, so redirect to login.
				response.sendRedirect("https://caermurcia.net:8443/contabilidad");
				//response.sendRedirect("https://192.168.0.170:8443/caerm");
				//response.sendRedirect("http://localhost:8090/contabilidad");
		
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void destroy() {
		//
	}
}