package com.geosystec.caerm.auth;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;






import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;





import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geosystec.caerm.entities.AuxUsuarios;
import com.geosystec.caerm.managers.RegistroServiceBean;
import com.geosystec.caerm.util.QueryParameter;




@ManagedBean(name = "user")
@SessionScoped
public class UserBean {

	static final long ONE_MINUTE_IN_MILLIS = 60000;// millisecs
	private String password;

	private String confirmedPassword;

	private int loggedIn;

	private String name;

	private Boolean director;
	
	private String tipo;
	private Boolean estado;
	private Boolean existenCambios = false;
	private String password1;
	private String passwordReset1;
	private String confirmedResetPassword;
	private String resetId;
	private String resetEmail;
	private String resetUser;

	private static Logger logger = LoggerFactory.getLogger(UserBean.class);


	public String getName() {
		return name;
	}

	public void setName(String newValue) {
		name = newValue;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String newValue) {
		password = newValue;
	}

	public String getResetEmail() {
		return resetEmail;
	}

	public void setResetEmail(String resetEmail) {
		this.resetEmail = resetEmail;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPasswordReset1() {
		return passwordReset1;
	}

	public void setPasswordReset1(String passwordReset1) {
		this.passwordReset1 = passwordReset1;
	}

	public String getConfirmedResetPassword() {
		return confirmedResetPassword;
	}

	public void setConfirmedResetPassword(String confirmedResetPassword) {
		this.confirmedResetPassword = confirmedResetPassword;
	}

	public String getConfirmedPassword() {
		return confirmedPassword;
	}

	public void setConfirmedPassword(String confirmedPassword) {
		this.confirmedPassword = confirmedPassword;
	}

	public Boolean getDirector() {
		return director;
	}

	public void setDirector(Boolean director) {
		this.director = director;
	}

	public String login() {
		try {
			doLogin();
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return "loginFailure";
		}
		
	
	if (loggedIn == 1) {
			logger.debug("ENTRANDO A LOGIN 1");
			return "main.xhtml";
		} 
		else
			return "loginFailure";
	}

	public String logout() {
		loggedIn = 0;

		return "login";
	}

	public void doLogin() throws NoSuchAlgorithmException {

		logger.debug("NAME ES: " + name);
		
		
			RegistroServiceBean contService = new RegistroServiceBean();

			

			List<AuxUsuarios> users = contService.findWithNamedQuery("findUserByLogin",QueryParameter.with("userName",name).parameters());
           			

			if ((users == null) || (users.isEmpty())) {
				logger.debug("ENTRA A USU NULL");
				loggedIn = 0;
				FacesMessage msg = new FacesMessage("USUARIO INCORRECTO", "");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {

				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
				byte claveEncriptada[] = md.digest();
				StringBuffer hexString = new StringBuffer();
				for (int i = 0; i < claveEncriptada.length; i++) {
					hexString.append(Integer
							.toHexString(0XFF & claveEncriptada[i]));
				}
				tipo = users.get(0).getTipo();
				logger.debug("CONTRASENA ENTRADA:" + hexString.toString());
				logger.debug("CONTRASENA:" + users.get(0).getPassword());
				if (users.get(0).getPassword().equalsIgnoreCase(hexString.toString())) {
					loggedIn = 1;
					logger.debug("LOG EXITO");
				} else {
					logger.debug("entra aqui en mal pass");
					FacesMessage msg = new FacesMessage("PASSWORD INCORRECTO",
							"");
					FacesContext.getCurrentInstance().addMessage(null, msg);

					loggedIn = 0;
				}
			}
		

	}

	
	
}
