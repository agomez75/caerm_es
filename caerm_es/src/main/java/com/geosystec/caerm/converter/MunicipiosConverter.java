package com.geosystec.caerm.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geosystec.caerm.entities.AuxMunicipios;
import com.geosystec.caerm.entities.AuxPaises;
import com.geosystec.caerm.managers.RegistroServiceBean;
import com.geosystec.caerm.util.QueryParameter;

@FacesConverter("muniConverter")
public class MunicipiosConverter implements Converter {

	private static Logger logger = LoggerFactory
			.getLogger(MunicipiosConverter.class);
	
	RegistroServiceBean registroService = new RegistroServiceBean();

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		
		AuxMunicipios titActual = null;
		logger.debug("SUBMITTED VALUE "+submittedValue);
		if (submittedValue != null) {
		logger.debug("ENTRANDO A CUENTAS CONVERTER");
		List<AuxMunicipios> provincias = registroService.findWithNamedQuery("findAllMunicipiosByNombre",QueryParameter.with("nombre",submittedValue).parameters());
		
		if (!provincias.isEmpty())
		    titActual = provincias.get(0);
		
		}
		return titActual;
	}
	@Override
	
	 public String getAsString(FacesContext fc, UIComponent uic, Object object) {
	        if(object != null) {
	        	return String.valueOf(((AuxMunicipios) object).getMuniDesc());
	        }
	        else {
	            return null;
	        }
	    }   

}
