package com.geosystec.caerm.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geosystec.caerm.entities.AuxPaises;
import com.geosystec.caerm.managers.RegistroServiceBean;
import com.geosystec.caerm.util.QueryParameter;

@FacesConverter("paisesConverter")
public class PaisesConverter implements Converter {

	private static Logger logger = LoggerFactory
			.getLogger(PaisesConverter.class);
	
	RegistroServiceBean registroService = new RegistroServiceBean();

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		
		AuxPaises titActual = null;
		logger.debug("SUBMITTED VALUE "+submittedValue);
		if (submittedValue != null) {
		logger.debug("ENTRANDO A CUENTAS CONVERTER");
		System.out.println("ENTRA EN CONVERTER PAIS ACTUAL"+submittedValue);
		List<AuxPaises> paises = registroService.findWithNamedQuery("findAllPaisesByNombre",QueryParameter.with("nombre",submittedValue).parameters());
		
		if (!paises.isEmpty())
		    titActual = paises.get(0);
		
		//System.out.println("ENTRA EN CONVERTER PAIS ACTUAL"+titActual.getPaisCod());
		
		
		
		}
		
		return titActual;
		
		
	}

	@Override
	
		 public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		        if(object != null) {
		        	return String.valueOf(((AuxPaises) object).getPaisDesc());
		        }
		        else {
		            return null;
		        }
		    }   
		

}
