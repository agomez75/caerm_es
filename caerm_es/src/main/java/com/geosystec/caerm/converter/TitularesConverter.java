package com.geosystec.caerm.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;







import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geosystec.caerm.entities.Titulares;
import com.geosystec.caerm.managers.RegistroServiceBean;
import com.geosystec.caerm.util.QueryParameter;

@FacesConverter("titularesConverter")
public class TitularesConverter implements Converter {

	private static Logger logger = LoggerFactory
			.getLogger(TitularesConverter.class);
	
	RegistroServiceBean registroService = new RegistroServiceBean();

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		Titulares titActual = new Titulares();
		logger.debug("SUBMITTED VALUE "+submittedValue);
		if (submittedValue != null) {
		logger.debug("ENTRANDO A CUENTAS CONVERTER");
		List<Titulares> cuentas = registroService.findWithNamedQuery("findAllTitularesByNombre",QueryParameter.with("codigo",'%'+submittedValue+'%').parameters());
		if (!cuentas.isEmpty())
		    titActual = cuentas.get(0);
		else
			titActual.setTitEmpresa(submittedValue);
		}

		logger.debug("ENTRANDO A CUENTAS CONVERTER FINAL "+submittedValue);
		logger.debug("ENTRA EN TIT ACTUAL TIT EMPRESA = "+titActual.getTitEmpresa());
	//logger.debug(cuentaActual.getCuentaNombre());
	//logger.debug(cuentaActual.getCuentaCodigo());
		
		registroService.destroy();

		return titActual;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent component,
			Object value) {
	
		return String.valueOf(((Titulares) value).getTitEmpresa());
	}

}
