package com.geosystec.caerm.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geosystec.caerm.entities.AuxClaseDoc;
import com.geosystec.caerm.entities.AuxMunicipios;
import com.geosystec.caerm.entities.AuxPaises;
import com.geosystec.caerm.entities.AuxProvincias;
import com.geosystec.caerm.entities.Registro;
import com.geosystec.caerm.entities.Titulares;
import com.geosystec.caerm.managers.RegistroServiceBean;
import com.geosystec.caerm.util.QueryParameter;




@ManagedBean(name = "registroBean")
@ViewScoped
public class RegistroBean {

	private RegistroServiceBean registroService;
	private List<Registro> registros = new ArrayList<Registro>();
	private List<Registro> registrosFiltered;
	private Registro editedRegistro = new Registro();
	private Registro selectedRegistro;
	private Boolean modify;
	private String nOrden;
	private Integer tipoRegistro;
	private boolean esEntrada = false;
	private Titulares remitenteSeleccionado;
	private List<AuxMunicipios> listMunicipios;
	private List<AuxProvincias> listProvincias;
	private List<AuxClaseDoc> listClaseDoc;
	private List<AuxPaises> listPaises;
	private AuxMunicipios municipio = new AuxMunicipios();
	private AuxPaises pais = new AuxPaises(); 
	private AuxProvincias provincia = new AuxProvincias(); 

	private static final Logger logger = LoggerFactory
			.getLogger(RegistroBean.class);

	@PostConstruct
	public void init() {
		logger.debug("Entra en Populate");
		pais.setPaisCod("ES");
		pais.setPaisDesc("ESPAÑA");
		populateRegistrosPais();
		populatePaises();
		RegistroServiceBean registroService = new RegistroServiceBean();
		editedRegistro.setAuxPaises((AuxPaises)registroService.find(AuxPaises.class, "ES"));
		editedRegistro.setAuxProvincias((AuxProvincias)registroService.find(AuxProvincias.class, 59));
		editedRegistro.setAuxMunicipios((AuxMunicipios)registroService.find(AuxMunicipios.class, 0));
		editedRegistro.setAuxClaseDoc((AuxClaseDoc)registroService.find(AuxClaseDoc.class, 5));
		registroService.destroy();
		populateProvincias();
		populateClaseDoc();
		provincia.setProvId(86);
		
		populateMunicipios();
	}

	public void populateRegistrosPais() {
		RequestContext requestContext = RequestContext.getCurrentInstance();
		//requestContext.execute("PF('registros').clearFilters()");
		registros.clear();
		logger.debug("Entra en Populate");
		
		if ( this.listProvincias != null)
			this.listProvincias.clear();
		if ( this.listMunicipios != null)
			this.listMunicipios.clear();
		if ( pais.getPaisCod() != null)
			this.populateProvincias();
		requestContext.update("tabRegistroES:formRegistro:registros:provtable");
		requestContext.update("tabRegistroES:formRegistro:registros:muntable");
		registroService = new RegistroServiceBean();
		logger.debug("PAIS ES = "+pais.getPaisCod());
		if ( pais.getPaisCod() == null)
			registros = registroService.findWithNamedQuery("findAllRegistros");
		else{
			logger.debug("REGISTROS SIZE = "+pais.getPaisDesc());
			registros = registroService.findWithNamedQuery("findAllRegistrosFilterPais",QueryParameter.with("pais", pais.getPaisCod())
					.parameters());
			logger.debug("REGISTROS SIZE = "+registros.size());
		}
		
		requestContext.update("tabRegistroES:formRegistro:registros");
		
		registroService.destroy();

	}
	
	public void populateRegistrosProv() {
		RequestContext requestContext = RequestContext.getCurrentInstance();
		// requestContext.execute("PF('registros').clearFilters()");
		//registros.clear();
		this.listMunicipios.clear();
		if ( provincia.getProvId() != 0)
			this.populateMunicipios();
		requestContext.update("tabRegistroES:formRegistro:registros:muntable");
		
		//logger.debug("Entra en Populate Provincia = "+provincia.getProvId());
		//AuxProvincias prov = getProvinciaId(pais.getPaisCod(),provincia.getProvCodprov());
		
		logger.debug("Entra en Populate PAIS= "+provincia.getProvId());
		registroService = new RegistroServiceBean();
		if( provincia.getProvId() != 0){
			registros = registroService.findWithNamedQuery("findAllRegistrosFilterPaisProv",
				QueryParameter.with("prov", provincia.getProvId()).parameters());
			logger.debug("REGISTROS SIZE A= "+registros.toString());
			logger.debug("REGISTROS SIZE A= "+registros.size());
			
		}else{
			registros = registroService.findWithNamedQuery("findAllRegistrosFilterPais",
					QueryParameter.with("pais",pais.getPaisCod()).parameters());
			logger.debug("REGISTROS SIZE B= "+registros.toString());
			logger.debug("REGISTROS SIZE B= "+registros.size());
			//requestContext.execute("PF('registros').filter()");
		}
			
		
		requestContext.update("tabRegistroES:formRegistro:registros");
		
		registroService.destroy();
		

		

	}
	
	/*public AuxProvincias getProvinciaId(String codPais, String codProv){
		registroService = new RegistroServiceBean();
		List<AuxProvincias> listProv = registroService.findWithNamedQuery("findAllProvinciaByPaisProvcod",
				QueryParameter.with("pais", pais.getPaisCod())
				.and("prov", provincia.getProvCodprov()).parameters());
		registroService.destroy();
		return listProv.get(0);
	}*/
	
	public void populateRegistrosMun() {
		RequestContext requestContext = RequestContext.getCurrentInstance();
		 requestContext.execute("PF('registros').clearFilters()");
		registros.clear();
		logger.debug("Entra en Populate");
		registroService = new RegistroServiceBean();
		if (municipio.getMuniDesc() == null) 
			registros = registroService.findWithNamedQuery("findAllRegistrosFilterPaisProv",
					QueryParameter.with("prov", provincia.getProvId()).parameters());
		else
			registros = registroService.findWithNamedQuery("findAllRegistrosFilterPaisProvMun",
					QueryParameter.with("pais", pais.getPaisCod())
					.and("prov", provincia.getProvId()).and("mun", municipio.getMuniDesc()).parameters());
		logger.debug("REGISTROS SIZE = "+registros.size());
		registroService.destroy();

	}
	
	public void saveOrModifyRegistro() {
		
		if (modify)
			modifyRegistro();
		else 
			saveRegistro();
		populateRegistrosPais();
		//RequestContext requestContext = RequestContext.getCurrentInstance();
		//requestContext.execute("PF('registros').clearFilters()");
	}

	public void modifyRegistro() {

		RegistroServiceBean registroService = new RegistroServiceBean();
		editedRegistro.setRegRemitente(this.remitenteSeleccionado.getTitEmpresa());
		registroService.update(editedRegistro);
		registroService.destroy();

	}

	public void saveRegistro() {

		RegistroServiceBean registroService = new RegistroServiceBean();

		editedRegistro.setRegNorden(nOrden);
		//gastosView.getEditedGasto().setAuxOrigenGasto((AuxOrigenGasto)gastosService.find(AuxOrigenGasto.class,gastosView.getOrigenGasto()));
		//editedRegistro.setAuxPaises((AuxPaises)registroService.find(AuxPaises.class, pais.getPaisCod()));
		//editedRegistro.setAuxProvincias((AuxProvincias)registroService.find(AuxProvincias.class, provincia.getProvId()));
		//logger.debug("ENTRA EN SAVE REGISTRO MUNICIPIO = "+municipio.getMuniId());
		//editedRegistro.setAuxMunicipios((AuxMunicipios)registroService.find(AuxMunicipios.class, municipio.getMuniId()));
		editedRegistro.setRegRemitente(this.remitenteSeleccionado.getTitEmpresa());
		registroService.create(editedRegistro);
		registroService.destroy();

	}


	public void doNew() {
		RegistroServiceBean registroService = new RegistroServiceBean();
		logger.debug("ENTRA EN DONEW");
		this.esEntrada = false;
		this.nOrden = null;
		editedRegistro = new Registro();
		editedRegistro.setAuxPaises((AuxPaises)registroService.find(AuxPaises.class, "ES"));
		editedRegistro.setAuxProvincias((AuxProvincias)registroService.find(AuxProvincias.class, 86));
		AuxMunicipios auxMun = new AuxMunicipios();
		auxMun.setMuniId(0);
		editedRegistro.setAuxMunicipios(auxMun);
		AuxClaseDoc auxClaseDoc = new AuxClaseDoc();
		auxClaseDoc.setClasedocId(5);
		editedRegistro.setAuxClaseDoc(auxClaseDoc);
		editedRegistro.setRegFechaEs(new Date());
		this.remitenteSeleccionado = new Titulares();
		modify = false;
		registroService.destroy();
		this.populateProvincias();
		this.populateMunicipios();
		
	}
	
	public void editar(){
		//editedRegistro = selectedRegistro;
		this.nOrden = selectedRegistro.getRegNorden();
		
		RegistroServiceBean regService = new RegistroServiceBean();
		
		
		List<Registro> registros = regService.findWithNamedQuery("findAllRegistrosByNordenES",
				QueryParameter.with("norden", nOrden ).and("origendest", selectedRegistro.getRegOrigendest()).parameters());
		
		editedRegistro = registros.get(0);
		
		

		List<Titulares> remitentes = new ArrayList<Titulares>();
		
		logger.debug("REG REMINTENTE = "+editedRegistro.getRegRemitente());
		
		remitentes = regService.findWithNamedQuery("findAllTitularesByNombre",
					QueryParameter.with("codigo", editedRegistro.getRegRemitente() ).parameters());
		
		if ( remitentes.size() > 0){
			remitenteSeleccionado = remitentes.get(0);
		}
		else{
			logger.debug("ENTRA EN SIZE == 0 TIOTULARES");
			remitenteSeleccionado = new Titulares();
			remitenteSeleccionado.setTitEmpresa(selectedRegistro.getRegRemitente());
		}
		
		editedRegistro.setAuxPaises(selectedRegistro.getAuxPaises());
		editedRegistro.setAuxProvincias(selectedRegistro.getAuxProvincias());
		editedRegistro.setAuxMunicipios(selectedRegistro.getAuxMunicipios());
		editedRegistro.setAuxClaseDoc(selectedRegistro.getAuxClaseDoc());
			

		regService.destroy();
		
		this.modify = true;
		
		
		
	}

	public List<Registro> getRegistros() {
		return registros;
	}

	public void setRegistros(List<Registro> registros) {
		this.registros = registros;
	}

	public Registro getEditedRegistro() {
		return editedRegistro;
	}

	public void setEditedRegistro(Registro editedRegistro) {
		this.editedRegistro = editedRegistro;
	}

	public Registro getSelectedRegistro() {
		return selectedRegistro;
	}

	public void setSelectedRegistro(Registro selectedRegistro) {
		this.selectedRegistro = selectedRegistro;
	}

	public Boolean getModify() {
		return modify;
	}

	public void setModify(Boolean modify) {
		this.modify = modify;
	}

	public Integer getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(Integer tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Titulares getRemitenteSeleccionado() {
		return remitenteSeleccionado;
	}

	public void setRemitenteSeleccionado(Titulares remitenteSeleccionado) {
		this.remitenteSeleccionado = remitenteSeleccionado;
	}

	public List<AuxMunicipios> getListMunicipios() {
		return listMunicipios;
	}

	public void setListMunicipios(List<AuxMunicipios> listMunicipios) {
		this.listMunicipios = listMunicipios;
	}

	public AuxMunicipios getMunicipio() {
		return municipio;
	}

	public void setMunicipio(AuxMunicipios municipio) {
		this.municipio = municipio;
	}

	public List<AuxProvincias> getListProvincias() {
		return listProvincias;
	}

	public void setListProvincias(List<AuxProvincias> listProvincias) {
		this.listProvincias = listProvincias;
	}

	public List<AuxPaises> getListPaises() {
		return listPaises;
	}

	public void setListPaises(List<AuxPaises> listPaises) {
		this.listPaises = listPaises;
	}

	public AuxPaises getPais() {
		return pais;
	}

	public void setPais(AuxPaises pais) {
		this.pais = pais;
	}

	public List<AuxClaseDoc> getListClaseDoc() {
		return listClaseDoc;
	}

	public void setListClaseDoc(List<AuxClaseDoc> listClaseDoc) {
		this.listClaseDoc = listClaseDoc;
	}

	public AuxProvincias getProvincia() {
		return provincia;
	}

	public void setProvincia(AuxProvincias provincia) {
		this.provincia = provincia;
	}

	public List<Registro> getRegistrosFiltered() {
		return registrosFiltered;
	}

	public void setRegistrosFiltered(List<Registro> registrosFiltered) {
		this.registrosFiltered = registrosFiltered;
	}

	public String getnOrden() {
		return nOrden;
	}

	public void setnOrden(String nOrden) {
		this.nOrden = nOrden;
	}

	public boolean isEsEntrada() {
		return esEntrada;
	}

	public void setEsEntrada(boolean esEntrada) {
		this.esEntrada = esEntrada;
	}

	public void retTipoRegistro() {
		this.tipoRegistro = editedRegistro.getRegOrigendest();
		if (tipoRegistro==0) {
			this.nOrden = this.getNumMaxOrden().toString();
			esEntrada = true;
			logger.debug("ENTRA EN TIPO REGISTRO esEntrada=" + esEntrada);
		} else {
			this.nOrden = this.getNumMaxOrden().toString();
			esEntrada = false;
			logger.debug("ENTRA EN TIPO REGISTRO esEntrada=" + esEntrada);
		}
	}
	
	public List<Titulares> completeRemitente(String query) {

		logger.debug("ESTA LLAMANDO AL COMPLETE REMITENTE " + query);

		RegistroServiceBean regService = new RegistroServiceBean();

		List<Titulares> remitentes;
		
		remitentes = regService.findWithNamedQuery("findAllTitularesByNombre",
					QueryParameter.with("codigo", '%' + query.toUpperCase() + '%').parameters());

		regService.destroy();

		logger.debug("EL TAMANO DE CUENTAS DESPUES DEL COMPLETE ES:" + remitentes.size());
		return remitentes;

	}
	
	


	private String getNumMaxOrden() {

		Integer sigNumReg;
		String maxNumReg;
		String numRegCompuesto;
		List<String> maxNumRegistro;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(cal.get(Calendar.YEAR),0,1,0,0,0);
		
		logger.debug("LA FECHA DEL DIA ="+cal.getTime());
		

		registroService = new RegistroServiceBean();
		maxNumRegistro = registroService.findWithNamedQuery("devuelveMaxNumRegistro",
				QueryParameter.with("tipodoc", this.tipoRegistro)
				.and("ejercicio", cal.getTime()).parameters());
		

		
		registroService.destroy();

		logger.debug("MAX NUM REG=" + maxNumRegistro.isEmpty());
		
		if (!maxNumRegistro.isEmpty() && maxNumRegistro.get(0) != null) {
			maxNumReg = maxNumRegistro.get(0);
			sigNumReg = new Integer(maxNumReg)+1;
			numRegCompuesto = componerNorden(sigNumReg);
		} else {
			sigNumReg = 1;
			numRegCompuesto = componerNorden(sigNumReg);
		}

		return (numRegCompuesto);
	}

	
	private String componerNorden(Integer nsec) {

		Integer ano;
		
		Calendar cal = Calendar.getInstance();
		ano = cal.get(Calendar.YEAR);
		

		String anoStr = ano.toString().substring(2, ano.toString().length());

		//nsec = nsec + 1;
		String nsecFormatted = String.format("%04d", nsec);

		String nOrden;
		
		nOrden = anoStr + "/" + nsecFormatted;
		return nOrden;
	}
	
	public void populateMunicipios(){
		logger.debug("ENTRA EN LISTA MUNICIPIOS EDITED REGISTRO NUM ORDEN = "+editedRegistro.getRegNorden());
		ArrayList<AuxMunicipios> auxMunicipios = new ArrayList<AuxMunicipios>();
		registroService = new RegistroServiceBean();
		if ( nOrden == null){
			logger.debug("ENTRA EN LISTA MUNICIPIOS 2 "+provincia.getProvId());
			listMunicipios = registroService.findWithNamedQuery("findAllMunicipiosByProvPais",
				QueryParameter.with("prov", provincia.getProvId())
				.and("pais", pais.getPaisCod()).parameters());
			
		}else{
			logger.debug("ENTRA EN LISTA MUNICIPIOS 3 "+editedRegistro.getAuxProvincias().getProvId());
			listMunicipios = registroService.findWithNamedQuery("findAllMunicipiosByProvPais",
					QueryParameter.with("prov", editedRegistro.getAuxProvincias().getProvId())
					.and("pais", editedRegistro.getAuxPaises().getPaisCod()).parameters());
			
		}
		registroService.destroy();
		
		
	}
	public void populatePaises(){
		ArrayList<AuxPaises> auxPaises = new ArrayList<AuxPaises>();
		registroService = new RegistroServiceBean();
		listPaises = registroService.findWithNamedQuery("findAllPaises");
		registroService.destroy();	
	}
	public void populateClaseDoc(){
		ArrayList<AuxClaseDoc> auxClaseDoc = new ArrayList<AuxClaseDoc>();
		registroService = new RegistroServiceBean();
		listClaseDoc = registroService.findWithNamedQuery("findAllClaseDoc");
		registroService.destroy();	
	}
	public void populateProvincias(){
		System.out.println("ENTRA EN POPULATE PROVINCIAS"+pais.getPaisDesc());
		ArrayList<AuxPaises> auxPaises = new ArrayList<AuxPaises>();
		registroService = new RegistroServiceBean();
		if ( editedRegistro.getRegNorden() == null){
			listProvincias = registroService.findWithNamedQuery("findAllProvinciasByPais",QueryParameter.with("pais", pais.getPaisCod())
				.parameters());
			//provincia.setProvId(86);
		}else{
			listProvincias = registroService.findWithNamedQuery("findAllProvinciasByPais",QueryParameter.with("pais", editedRegistro.getAuxPaises().getPaisCod())
					.parameters());
		}
		logger.debug("ENTRA EN LISTA PROVINCIAS SIZE= "+listProvincias.size());
		listMunicipios =  new ArrayList<AuxMunicipios>();
		registroService.destroy();	
	}
	

}
