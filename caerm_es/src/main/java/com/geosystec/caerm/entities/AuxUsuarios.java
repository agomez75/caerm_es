package com.geosystec.caerm.entities;

// Generated 16-feb-2015 10:08:36 by Hibernate Tools 4.0.0

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * AuxUsuarios generated by hbm2java
 */
@NamedQueries({
	@NamedQuery(name = "findUserByLogin", query = "select usu from AuxUsuarios usu where usu.login like :userName")
})
@Entity
@Table(name = "aux_usuarios", schema = "aux")
public class AuxUsuarios implements java.io.Serializable {

	private String login;
	private String password;
	private String nombre;
	private String tipo;
	private Boolean estado;

	public AuxUsuarios() {
	}

	public AuxUsuarios(String login) {
		this.login = login;
	}

	public AuxUsuarios(String login, String password, String nombre,
			String tipo, Boolean estado) {
		this.login = login;
		this.password = password;
		this.nombre = nombre;
		this.tipo = tipo;
		this.estado = estado;
	}

	@Id
	@Column(name = "login", unique = true, nullable = false)
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "password")
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "nombre", length = 100)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "tipo", length = 1)
	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Column(name = "estado")
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}
