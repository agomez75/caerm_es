package com.geosystec.caerm.entities;

// Generated 21-abr-2016 10:58:41 by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * AuxMunicipios generated by hbm2java
 */
@NamedQueries({
	@NamedQuery(name = "findAllMunicipios", query = " select muni from AuxMunicipios muni order by muni.muniDesc"),
	@NamedQuery(name = "findAllMunicipiosByProvPais", query = " select muni from AuxMunicipios muni where muni.muniProv=:prov and muni.muniPais=:pais order by muni.muniDesc"),
	@NamedQuery(name = "findAllMunicipiosByNombre", query = " select muni from AuxMunicipios muni where muni.muniDesc like :nombre"),
})
@Entity
@Table(name = "aux_municipios", schema = "registro")
public class AuxMunicipios implements java.io.Serializable {

	private int muniId;
	private String muniPais;
	private Integer muniProv;
	private String muniCod;
	private String muniDesc;
	private Set<Registro> registros = new HashSet<Registro>(0);

	public AuxMunicipios() {
	}

	public AuxMunicipios(int muniId) {
		this.muniId = muniId;
	}

	public AuxMunicipios(int muniId, String muniPais, Integer muniProv,
			String muniCod, String muniDesc, Set<Registro> registros) {
		this.muniId = muniId;
		this.muniPais = muniPais;
		this.muniProv = muniProv;
		this.muniCod = muniCod;
		this.muniDesc = muniDesc;
		this.registros = registros;
	}

	@Id
	@Column(name = "muni_id", unique = true, nullable = false)
	public int getMuniId() {
		return this.muniId;
	}

	public void setMuniId(int muniId) {
		this.muniId = muniId;
	}

	@Column(name = "muni_pais", length = 2)
	public String getMuniPais() {
		return this.muniPais;
	}

	public void setMuniPais(String muniPais) {
		this.muniPais = muniPais;
	}

	@Column(name = "muni_prov")
	public Integer getMuniProv() {
		return this.muniProv;
	}

	public void setMuniProv(Integer muniProv) {
		this.muniProv = muniProv;
	}

	@Column(name = "muni_cod", length = 3)
	public String getMuniCod() {
		return this.muniCod;
	}

	public void setMuniCod(String muniCod) {
		this.muniCod = muniCod;
	}

	@Column(name = "muni_desc", length = 30)
	public String getMuniDesc() {
		return this.muniDesc;
	}

	public void setMuniDesc(String muniDesc) {
		this.muniDesc = muniDesc;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "auxMunicipios")
	public Set<Registro> getRegistros() {
		return this.registros;
	}

	public void setRegistros(Set<Registro> registros) {
		this.registros = registros;
	}

}
